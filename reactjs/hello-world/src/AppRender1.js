import React, {Component} from "react"
import Conditional from './components/Conditional'

class AppRender1 extends Component {
    constructor() {
        super()
        this.state = {
            isLoading : true,
            unreadMessage : ["a", "b"]
        }
    }
    componentDidMount() {
        setTimeout( () => {
            this.setState({
                isLoading : false
            })
        }, 1500)
    }
    render() {
        return (
            <div>
                {this.state.isLoading ? <h1> Loading </h1> : 
                <Conditional isLoading = {this.state.isLoading}/> }
                {this.state.unreadMessage.length >0 ? 
                    <h1> unread message  - {this.state.unreadMessage.length}</h1>  : null }
            
                {this.state.unreadMessage.length >0  && 
                    <h1> unread Message 2 - {this.state.unreadMessage.length}</h1> }
            </div>
        )
    }
}
export default AppRender1
