import React, {Component, useState, useEffect} from "react"
import randomcolor from "randomcolor"


function AppHookCount() {
    const [count, setCount] = useState(1)
    const [answer, setAnswer] = useState("YES")
    const [color, setColor] = useState("")
    
    function decrement() {
        return(
            setCount (prevCount => prevCount -1 )
        )
    }
    useEffect (() => {
        const intervalId = setInterval(()=> { setCount (prevCount => prevCount +1)},3000)
        //setColor(randomcolor())
        return() => clearInterval(intervalId)
    
    }, [])
   
    useEffect (() => {
        setColor(randomcolor())
        setInterval(()=> { setColor(randomcolor())},1000)
    
    }, [count])
    return(
        <div>
            
            <p style = {{color: color}}>{count} </p>
            <button onClick = {() => setCount(prevCount => prevCount + 1)}>Increment</button>
            <button onClick = {decrement}>decrement</button>
        </div>
    )
    
}


// class AppHookCount extends Component {
//     constructor(){
//         super()
//         this.state = {
//             count : 0
//         }
//     }
//     render(){
//         return(
//             <div>
//                 <p>How many {this.state.count} </p>
//                 <button>change</button>
//             </div>
//         )
//     }
// }

export default AppHookCount