import React, {Component} from "react"
import Header from './Header'

class MemeGenerator extends Component {
    
    constructor(){
        super()
        this.state = {
            topText : "",
            bottomText : "",
            randomImg : "http://i.imgflip.com/1bij.jpg",
            allMemeImgs : []
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit= this.handleSubmit.bind(this)
    }
    componentDidMount(){
        // use nativ function fetch 
        //returns promise we need to turn to our script object
        // we call json method on that and then we got act response useful for us
        // then pull memes array from response.data
        // then save it to property allMemesImgs
        // setstate without caring about previous state
                
        fetch("https://api.imgflip.com/get_memes")
            .then(response => response.json())
            .then( response => { const {memes} = response.data
                this.setState ({
                    allMemeImgs : memes
                })
            })   
    }
    handleChange(event) {
        const {type,value,checked,name} = event.target
        this.setState({
            [name] : value
        })
    }

    handleSubmit(event) {
        event.preventDefault()
        const randNum = Math.floor(Math.random() * this.state.allMemeImgs.length)
        this.setState({randomImg :  this.state.allMemeImgs[randNum].url })

    }

    render() {
        return (
            //className = "meme-form"
            <div>
                <form  className = "meme-form" onSubmit = {this.handleSubmit}>
                    <input 
                        type = "text"
                        name = "topText"
                        placeholder = "top text"
                        value = {this.state.topText}
                        onChange = {this.handleChange}
                    />
                    <br/>
                    
                    <input 
                        type = "text"
                        name = "bottomText"
                        placeholder = "bottom text"
                        value = {this.state.bottomText}
                        onChange = {this.handleChange}
                    />
                    <br/>

                    <p>top text : {this.state.topText}</p>
                    <p>bottom text : {this.state.bottomText}</p>

                    <button onclick>GEN</button>
                    
                </form>
                <div>
                    <img src = {this.state.randomImg} alt= ""/>
                    <h1 className="top">{this.state.topText}</h1>
                    <h1 className="bottom">{this.state.bottomText}</h1>
                </div>
            </div>
        )
    }
}

export default MemeGenerator

    /**
     * We'll be using an API that provides a bunch of meme images.
     * 
     * Your task:
     * make an API call to "https://api.imgflip.com/get_memes" and save the 
     * data that comes back (`response.data.memes`) to a new state property
     * called `allMemeImgs`. (The data that comes back is an array)
     */