import React from "react"
import Trollface from "./Trollface.png"
import Style from "./style.css"

function Header () {
    return (
        <header>
            <img
                src = {Trollface}
                alt="Problem?"
                width ="100"
                height = "100"
            />
             <p>header</p>
        </header>
    )
}

export default Header