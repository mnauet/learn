import React, {useState} from 'react';
import {View, Text,  StyleSheet, FlatList, Alert} from 'react-native';
import Header from './components/Header';
import ListItem from './components/ListItem';
import AddItem from './components/AddItem';
import {v4 as uuidv4} from 'uuid';


const App = () => {
  const [items, setItems] = useState([
    {id: 1, text: 'Milk'},
    {id: 2, text: 'Eggs'},
    {id: 3, text: 'Bread'},
    {id: 4, text: 'Juice'}

  ]);

  const deleteItem = (id) => {
    setItems(prevItems => {
      return prevItems.filter(item => item.id != id);
    });
  }
const addItem = (text) => {
  if (!text) {
    Alert.alert('Error', 'Please Add an Item');

  } else {
    setItems(prevItems => {
      const newItem = [{id:uuidv4(), text}, ...prevItems];
      return newItem
    });
  }

};

  return (
    <View style= {styles.container}>
      <Header title='Shopping List'></Header>
      <AddItem addItem={addItem}></AddItem>
      <FlatList 
        data={items}
        renderItem={({item}) => (
          <ListItem item={item} deleteItem={deleteItem}  />
        )}

      />
    </View>
  );
};

Header.defaultProps = {
  title: 'Shopping List Default'
}

const styles = StyleSheet.create({
  container: {
    flex: 1, 
  },
});
export default App;
