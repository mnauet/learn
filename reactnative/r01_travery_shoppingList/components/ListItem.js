import React from 'react';
import {View, Text,  StyleSheet,TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

const ListItem = ({item, deleteItem}) => {
  return (
      <TouchableOpacity style={styles.listItem}>
        <View style={styles.listItemView}>
        <Text style={styles.listItemText} > {item.text} </Text>
        <Icon 
            name="remove" 
            size={20} 
            color="firebrick" 
            onPress= { () => deleteItem(item.id)}>
        </Icon>
        </View>
      </TouchableOpacity>

  );
};
const styles = StyleSheet.create({
    listItem: {
        padding: 15,
        backgroundColor: '#f8f8f0',
        borderBottomWidth: 2,
        borderColor: '#eee',
    },
    listItemView: {
        flexDirection: 'row',
        justifyContent: 'space-between',

    },
    listItemText: {
        fontSize: 23,
    },
 
});
export default ListItem;
