//  rsf
import React from 'react';
import { ImageBackground, StyleSheet, View, Image, Text } from 'react-native';

function WellcomScreen(props) {
    return (
        <ImageBackground 
            style= {styles.background}
            source={require('../assets/icon.png')}>
            <View style={styles.logoContainer}>
                <Image source={require('../assets/favicon.png')} 
                    style={styles.logo}/>
                <Text>Sell what you don't need</Text>
            </View>
            <View style= {styles.loginButton}/>
            <View style= {styles.registerButton}/>
        </ImageBackground>

    );
}
// rnss react native style sheet
const styles = StyleSheet.create({
    background: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',

    },
    loginButton: {
        width: "100%",
        height: 70,

        backgroundColor: '#fc5c65',
    },    
    registerButton: {
        width: "100%",
        height: 70,

        backgroundColor: '#4ecdc4',
    },
    logo: {
        width: 100,
        height: 100,
        backgroundColor: '#4ecdc4',
    },
    logoContainer: {
        position: 'absolute',
        top: 50,
        alignItems: 'center',
    },   
})
export default WellcomScreen;