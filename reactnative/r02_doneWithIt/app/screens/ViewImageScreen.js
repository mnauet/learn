// rnf

import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'

import colors from '../config/colors'

function ViewImageScreen(props) {
    return (
        <View style={styles.container}>
            <View style={styles.closeIcon}></View>
            <View style={styles.deleteIcon}></View>
            <Image 
                style={styles.image}
                resizeMode= 'contain'
                source= {require('../assets/splash.png')} 
            />
        </View>
        )
}

// rnss
const styles = StyleSheet.create({
    closeIcon: {
        width: 50,
        height: 50,
        backgroundColor: colors.primary,
        position: 'absolute',
        top: 50,
        left: 50,

    },
    container: {
        backgroundColor: colors.secondary,
        flex: 1,

    },
    deleteIcon: {
        width: 50,
        height: 50,
        backgroundColor: '#4ecdc4',
        position: 'absolute',
        top: 50,
        right: 50,
    },
    image: {
        width: "100%",
        height: "100%",
        backgroundColor: 'lightgreen',
    }
    
})

export default ViewImageScreen;
