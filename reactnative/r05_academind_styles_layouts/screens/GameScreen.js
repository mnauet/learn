import React, {useState, useRef, useEffect} from 'react';
import { View, Text, StyleSheet, Button, Alert, ListView } from 'react-native';

const generateRandomBetween = (min, max, exclude) =>{
    min = Math.ceil(min);
    max = Math.floor(max);
    const rndNum = Math.floor(Math.random() * (max-min)) + min;
    if (rndNum === exclude){
        return generateRandomBetween(min,max, exclude); 
        }
    else {
        return rndNum;
    }
};

const GameScreen = (props) => {
    const [currentGuess, setCurrentGuess] = useState(generateRandomBetween(1,100, props.userChoice));
    const currentLow = useRef(1);
    const currentHigh = useRef(100);
    const [rounds, setRounds]= useState(1);
    
    const {userChoice, onGameOver} = props;
    useEffect( ()=>{ // runs at end of each rendering
        if (currentGuess === props.userChoice){
            props.onGameOver(rounds);
            console.log('Game over');
        }
    }, [currentGuess, userChoice, onGameOver]);
    console.log(currentGuess);

    const nextGuessHandler = direction => {
        if ((direction === 'lower' && currentGuess < props.userChoice) || (direction === 'greater' && currentGuess > props.userChoice)){
            Alert.alert('Don\'t lie and share the right direction', [{text: 'Sorry', style: 'cancel'}]);
            console.log('wrong direction');
            return;
            };
        if (direction === 'lower') { // get lower
            currentHigh.current = currentGuess;
        } else { // get greater number , go up
            currentLow.current = currentGuess;
        }
        const nextNumber = generateRandomBetween(currentLow.current, currentHigh.current, currentGuess);
        console.log('New Guess', nextNumber)
        setCurrentGuess(nextNumber);
        setRounds(curRounds => curRounds + 1);
        console.log('rounds', rounds);
 
    };
    return (
        <View style= {styles.screen}>
            <Text>Opponent's Guess</Text>
            <Text>{currentGuess}</Text>
            <View style={styles.buttonContainer}>
                <View>
                    <Button title='LOWER' onPress={nextGuessHandler.bind(this, 'lower')}/>
                </View>
                <View>
                    <Button title='GREATER' onPress={nextGuessHandler.bind(this, 'greater')}/>
                </View>
            </View>
            <View>
                <Text>Round Number: {rounds}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({ 
    screen: {
        flex: 1,
        padding: 10,
        alignItems: 'center',

    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: 20,
        width: 300,
        maxWidth: '80%',

    },
    
})
export default GameScreen
