import React, {useState} from 'react';
import { View, Text, StyleSheet, TextInput, Button, TouchableWithoutFeedback, Keyboard, Alert  } from 'react-native';

import Card from '../components/Card';
import Input from '../components/Input';
import Colors from '../constants/colors'

const StartGameScreen = ({onStartGame}) => {
    const [enteredValue, setEnteredValue] = useState('');
    const [confirmed, setConfirmed] = useState(false);
    const [selectedNumber, setSelectedNumber] = useState(false);

    const numberInputHandler = inputText => {
        setEnteredValue(inputText.replace(/[^0-9]/g, ''));
    }
    const resetInputHandler = () => {
        setEnteredValue('');
        setConfirmed(false);
    }
    const confirmInputHandler = () => {
        const choosenNumber = parseInt(enteredValue)
        if (isNaN(choosenNumber)  || choosenNumber <=1 || choosenNumber >99 ) {
            Alert.alert('Invalid Number', 'Please enter a valid number between 0 and 99', 
                [{text: 'Okay', style:'destructive', onPress: resetInputHandler}]);
            console.log('invalid input')
            return;
        };
        setConfirmed(true);
        setSelectedNumber(choosenNumber);
        setEnteredValue('');
        console.log(choosenNumber);
    }

    let confirmedOutput ;
    if(confirmed) {
        confirmedOutput = (
            <View style={styles.summaryContainer}> 
                <Text > Selected Number </Text>
                <Text style={styles.number}> {selectedNumber}</Text>
                <View>
                    <Button title='Start Game' onPress={()=>onStartGame(selectedNumber)}/>
                </View>
            </View>
            ); 
    };

    return (
        <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss}>
            <View style= {styles.container}>
                <Text style={styles.titleText}>Start a New Game</Text>
                <View style={styles.inputContainer}>
                    <Text style={styles.numberText}>Select a Number</Text>
                    <TextInput 
                        style={styles.input} 
                        placeholder='Enter a Number!' 
                        keyboardType="number-pad" 
                        maxLength={2} blurOnSubmit
                        blurOnSubmit
                        autoCapitalize= "none"
                        autoCorrect
                        onChangeText= {numberInputHandler}
                        value={enteredValue} >

                    </TextInput>

                    <View style={styles.buttonContainer}>
                        <View style={styles.button}>
                            <Button title='Reset' color={Colors.secondary}  onPress={resetInputHandler}/>
                        </View>
                        <View style={styles.button}>
                            <Button title='Confirm' color={Colors.primary }  onPress={confirmInputHandler}/> 
                        </View>
                    </View>
                </View>
                {confirmedOutput}
            </View>
            
        </TouchableWithoutFeedback>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding:10,
        alignItems: 'center',
      
    },
    titleText: {
        fontSize: 20,
        marginVertical: 10,

    },
    numberText: {
        fontSize: 20,
        marginVertical: 10,
    },
    inputContainer: {
        width: 300,
        maxWidth: '80%',
        alignItems: 'center',

        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: {width: 0, height:2},
        shadowRadius: 0,
        backgroundColor: 'white',
        elevation: 5,
        padding: 20,
        borderRadius: 20,   
    },
    input:{
        width: 120,
        textAlign: 'center',

        height: 30,
        borderBottomColor: 'grey',
        borderBottomWidth: 1,
        marginVertical: 10,
    },
    buttonContainer: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-around',
        paddingHorizontal: 15,
    },
    button: {
        width: '50%',
        margin: 1,
    },
    summaryContainer: {
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: {width: 0, height:2},
        shadowRadius: 0,
        backgroundColor: 'white',
        elevation: 5,
        padding: 20,
        borderRadius: 20,   

        padding:10,
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical:10,
    },
    number: {
        color: Colors.secondary,
        fontSize:22,
        marginVertical:10,
        borderColor: Colors.secondary,
        borderWidth: 1,
        alignItems: 'center',

    },

    
})

export default StartGameScreen
