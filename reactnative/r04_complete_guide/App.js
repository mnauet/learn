
import React, {useState} from 'react';
import { StyleSheet, View, Button, FlatList } from 'react-native';

import GoalItem from './components/GoalItem';
import GoalInput from './components/GoalInput';

export default function App() {
  const [courseGoals, setCourseGoals] = useState([]);
  const [isAddMode, setIsAddMode] = useState(false);
  
  // function goalInputHandler(enteredText) {
  const addGoalHandler = (goalTitle) => {
    console.log(goalTitle);
    setCourseGoals( currentGoals => [
      ...courseGoals, 
      {id: Math.floor(Math.random()*10000).toString(), value: goalTitle}]);
      setIsAddMode(false);
    console.log(courseGoals);
  }
const cancelModal = () => {
    setIsAddMode(false);
} 
const deleteHandler = (id) => {
  console.log('item with id pressed', id);
  setCourseGoals(prevItems => {
    return prevItems.filter(item => item.id != id);
  });
  }

  return (
    <View style= {styles.screen}>
      <Button title='Add New Goal' onPress= {()=>setIsAddMode(true)} />
      <GoalInput visible={isAddMode} onAddGoal={addGoalHandler} onCancelGoal= {cancelModal}/>
      <FlatList 
        keyExtractor={(item, index) => item.id}
        data={courseGoals}
        renderItem={(itemData) => (
          <GoalItem item={itemData.item} onDelete={() => deleteHandler(itemData.item.id)}/>
                          // onPress= {() => onAddGoal(enteredGoal)}/>
                          // onPress={ onAddGoal.bind(this, enteredGoal)}/>
        )
        }
      />
    </View>
  );
}

const styles = StyleSheet.create({
  btn: {
    color: 'red',
  },
  screen: {
    left: 20,
    padding: 20
  },



 
});