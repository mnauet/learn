import React, {useState} from 'react'
import { View, Text, StyleSheet, TextInput, Button } from 'react-native'
import Modal from '../Modal';

const GoalInput = ({visible,onAddGoal, onCancelGoal}) => {
    const [enteredGoal, setEnteredGoal] = useState('');
    
    const  goalInputHandler = (enteredText) => {
        setEnteredGoal(enteredText);
      }
    const addGoal = () => { 
        onAddGoal(enteredGoal); 
        setEnteredGoal('');
    };

    return (
        <Modal visible={visible} animationType='slide'>
            <View style={styles.inputContainer}  >
                <TextInput 
                    placeholder='Add Text'
                    style= {styles.textInput}
                    onChangeText={goalInputHandler}
                    value={enteredGoal}/>
                <View style={styles.btnView}>
                    <View style={styles.btn}>
                        <Button title='Cancel' color= 'red' onPress={onCancelGoal}/>
                    </View>
                    <View style={styles.btn}>
                        <Button  title= 'Add'  color='green'
                            //onPress= {() => onAddGoal(enteredGoal)}/>
                            // onPress = {onAddgoal.bind(this, enteredGoal)}
                            onPress={ addGoal }/>
                    </View>
   
                </View>
            </View>
        </Modal>
    )
}
const styles = StyleSheet.create({
    inputContainer: {
        flex:1,
        left:20,
        padding:20,
        width: '80%',
        justifyContent: 'center',
        alignItems: 'flex-start',
      },
      textInput: {
        borderColor: '#ccc', 
        borderWidth: 1,
        width: '100%',
        height:30,
      },
      btn: {
          margin: 1,
          width: '50%',
      },
      btnView: {
        marginTop: 10,
        flexDirection: 'row', 
        justifyContent: 'space-around',
        width: '100%',
        height: 30,
      },
})

export default GoalInput
