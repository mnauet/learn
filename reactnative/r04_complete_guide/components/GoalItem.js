// rnfe
import React from 'react'
import { View, Text, StyleSheet,TouchableOpacity } from 'react-native'
//import Icon from 'react-native-vector-icons/dist/FontAwesome';

const GoalItem = ({item, onDelete}) => {
    return (
      <TouchableOpacity activeOpacity={0.8} onLongPress={onDelete}>
        <View  style={styles.listItem} > 
          <Text> {item.value}</Text>
        </View> 
      </TouchableOpacity>
    )
}
//rnss
const styles = StyleSheet.create({
    listItem: {
        padding: 10,
        marginVertical: 1,
        backgroundColor: '#ccc',
        borderColor: 'black',
        borderWidth: 1,
        width: '810%'
      } 
})

export default GoalItem;
